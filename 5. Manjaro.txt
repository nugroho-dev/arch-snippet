###Turn Off Touchpad When USB Mouse is Plugged 

sudo nano /usr/local/bin/touchpad

#!/bin/bash
list=`xinput --list | grep -i 'mouse'`
if [ ${#list} -eq 0 ]; then
    exec `synclient touchpadoff=0`
    notify-send "No USB mouse found" "Your touchpad is set to ON"
else
    exec  `synclient touchpadoff=1`
    notify-send "USB mouse plugged" "Your touchpad is now turned OFF"
fi

sudo chmod +x /usr/local/bin/touchpad

sudo nano /etc/udev/rules.d/01-touchpad.rules

SUBSYSTEM=="input", KERNEL=="mouse[0-9]*", ACTION=="add", ENV{DISPLAY}=":0", ENV{XAUTHORITY}="/home/username/.Xauthority", RUN+="/usr/local/bin/touchpad"
SUBSYSTEM=="input", KERNEL=="mouse[0-9]*", ACTION=="remove", ENV{DISPLAY}=":0", ENV{XAUTHORITY}="/home/username/.Xauthority", RUN+="/usr/local/bin/touchpad"

Settings>Session and Startup>Application Autostart