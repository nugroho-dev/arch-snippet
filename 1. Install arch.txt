1. 	cfdisk

1b. # lsblk
	# cgdisk /dev/sda

2. 	mkfs.ext4 /dev/sda1
	mkswap /dev/sda2
	mount /dev/sda1 /mnt
	swapon /dev/sda2
	
3a.	nano /etc/pacman.conf
	SigLevel = Never

3b.	pacstrap /mnt base base-devel grub-bios

4.	# genfstab -U -p /mnt >> /mnt/etc/fstab
	# arch-chroot /mnt

5.	# ln -s /usr/share/zoneinfo/Asia/Jakarta /etc/localtime

6.	# nano /etc/locale.gen
	(uncomment # pada en_US.UTF-8)

7.	# nano /etc/locale.conf
	LANG="en_US.UTF-8"
	LC_COLLATE="C"
	LC_TIME="en_US.UTF-8"

8.	# locale-gen

9.  # echo myhostname > /etc/hostname

10. # mkinitcpio -p linux

11. # nano /etc/default/grub
    (Add GRUB_DISABLE_SUBMENU=y at the end of /etc/default/grub)
	GRUB_TIMEOUT=5 (Diganti Terserah)

	#grub-mkconfig -o /boot/grub/grub.cfg
	#grub-install --recheck /dev/sda

12. exit
	umount /mnt
	reboot

13. systemctl start dhcpcd
	systemctl enable dhcpcd

14. pacman -S xorg-server ttf-dejavu

15. lspci | grep VGA
https://wiki.archlinux.org/index.php/ATI
	pacman -S xf86-video-ati
	

16. pacman -S xfce4 xfce4-goodies slim gvfs
	(*gvfs dibutuhkan thunar untuk auto-mount drives seperti usb dan external hard drives.)

17. systemctl enable slim

18. cp /etc/skel/.xinitrc ~/.xinitrc
	nano ~/.xinitrc
	(hilangkan komentar "exec startxfce4")

19. pacman -S xf86-input-synaptics
	(*khusus laptop untuk Touchpad-nya)

20. Untuk konfigurasi sensitivity Touchpad
	nano /etc/X11/xorg.conf.d/50-synaptics.conf
	(Cari Section "InputClass" dan tambahkan opsi berikut: )
	Option "FingerHigh" "8"